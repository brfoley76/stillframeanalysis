setwd("/media/brad/Data/Documents/VideoAnalysis/stillFrames/stillAGit")
library(lme4)

fDat=read.table("fDat.csv", sep=",", as.is=T, header=T)
mDat=read.table("mDat.csv", sep=",", as.is=T, header=T)

#because of either entry error or too many flies, offF and offM have a (very few) negative entries
#I'm gonna fudge those for the moment


fDat[fDat[,"offF"]<0,"offF"]=0
fDat[fDat[,"offM"]<0,"offM"]=0
mDat[mDat[,"offF"]<0,"offF"]=0
mDat[mDat[,"offM"]<0,"offM"]=0

columnNames=c("modName", "model", "garbage", "AIC", "BIC", "logLik", "deviance", "dfResid")
bestFModel=read.table("resultsOfModelSelF.txt", sep=",", header=F)
bestFModel=bestFModel[,1:8]
names(bestFModel)=columnNames
bestMModel=read.table("resultsOfModelSelM.txt", sep=",", header=F)
bestMModel=bestMModel[,1:8]
names(bestMModel)=columnNames

bestFModel=bestFModel[order(bestFModel[,"BIC"]),]
bestF=glmer(leave~nf + log(pf) + offF + (1 | genf) + (log(pm + 1) | genm) + period, data=fDat, weights=weight, family="binomial")
#wait a minute# 
bestMModel=bestMModel[order(bestMModel[,"BIC"]),]
bestM=glmer(leave~nf + fPerM + (1 | genf) + (pm | genm) + period, data=mDat, weights=weight, family="binomial")
#wait a minute# 
bestM
coef(bestM)
ranef(bestM)
plot(bestM)

bestF
coef(bestF)
ranef(bestF)
plot(bestF)
