###################################################################################
###### This goes through and corrects the output files relative to the readme files 
###### and gives a list of every file in which those were not concordant
###### be careful because it overwrites the clickit files
###### and if the readmes were wrong, you might screw something up in a way that's complicated to fix
###### so make a temp backup of all the clickit files first



setwd("/media/brad/Data/Documents/VideoAnalysis/stillFrames/stillAGit")
myData=read.table("finalByView.csv", sep=",", header=T, as.is=T) #I think this is right?

head(myData[which(myData[,"month"]==month & myData[,"day"]==day & myData[,"station"]==station),1:14])

outputVec=c("month", "day", "period", "view", "station", "fgeno", "mgeno", "fcol", "mcol", "nf", "nm", "frame", "sex", "x", "y")


processReadme=function(myReadme){
  date=myReadme[2]
  date=unlist(strsplit(date, "_"))
  fgeno=toupper(myReadme[5])
  mgeno=toupper(myReadme[9])
  fcol=tolower(myReadme[7])
  fcol=substring(fcol, 1, 1)
  mcol=tolower(myReadme[11])
  mcol=substring(mcol, 1, 1)
  nf=as.numeric(myReadme[8])
  station=as.numeric(myReadme[4])
  year=as.numeric(date[1])
  month=as.numeric(date[2])
  day=as.numeric(date[3])
  readmeData=data.frame(month, day, station, fgeno, mgeno, fcol, mcol, nf)  
  
  return(readmeData)
}

hourlyRates=function(currentFile, readmeData, colNames){
  discRepant=c()
  periodPieces=c("am1", "am2", "pm1", "pm2")
  splitWad=strsplit(currentFile, "/")
  period=splitWad[[1]][1]
  period=which(periodPieces==period) #1:4
  viewInfo=splitWad[[1]][2]
  viewNumber=strsplit(viewInfo, "_")[[1]][2]
  viewNumber=as.numeric(substring(viewNumber,5, 5))
  classesvec=c(rep("integer", 6), rep("character", 4), rep("integer", 5), rep("numeric", 2))
  fileData=read.table(currentFile, sep=",", header=F)
  if(fileData[1,1] != "dummy"){
    fileData=read.table(currentFile, sep=",", colClasses=classesvec)
    colnames(fileData)=colNames
    viewData=c(period, viewNumber)
    checkSumsNames=c("month", "day", "station", "fgeno", "mgeno", "fcol", "mcol", "nf")
    checkMetaNames=c("period","view")
    checksums=fileData[1,checkSumsNames]
    checkMeta=fileData[1,checkMetaNames]
    
    if(sum(checksums != readmeData) > 0 | sum(checkMeta != viewData) > 0){
      discRepant=fileData[1,]
      fileData[,checkSumsNames]=readmeData[checkSumsNames]
      fileData[,checkMetaNames]=checkMeta[checkMetaNames]
      fileData[,"nm"]=20-fileData[,"nf"]
      discRepant=rbind(discRepant, fileData[1,])
      discRepant[2,1]=2
      
    }
    write.table(fileData, currentFile, sep=",", row.names=F, quote=F)
  }
  return(discRepant)
}


deviations=c()
firstDir="/media/brad/Data/Documents/VideoAnalysis/stillFrames/stillframeanalysis/clickitOut/station_3"
setwd(firstDir)
dayFolders=list.dirs(path = ".", recursive = F)

setwd(firstDir)

colNames=c("dummy", "month", "day", "period", "view", "station", "fgeno", "mgeno", "fcol", "mcol", "nf", "nm", "frame", "something", "sex", "x", "y")

for(i in 1:length(dayFolders)){
  setwd(dayFolders[i])
  myFileList=list.files(recursive=F)
  myFileList=grep("readme", myFileList, value=T)
  myReadme=scan(myFileList[1], sep=" ", what=character())
  readmeData=processReadme(myReadme)
  #head(myData[which(myData[,"month"]==month & myData[,"day"]==day & myData[,"station"]==station),1:14])
  outputFiles=list.files(path=".", recursive=T)
  outputFiles=grep("View", outputFiles, value=T)
  outputFiles=grep("csv", outputFiles, value=T)
  discRepancy=do.call(rbind, lapply(1:(length(outputFiles)), function(k) {hourlyRates(currentFile=outputFiles[k], readmeData, colNames)}))
  deviations=rbind(deviations, discRepancy)
  setwd(firstDir)
}



setwd("/media/brad/Data/Documents/VideoAnalysis/stillFrames/stillframeAnalysis/stillAGit")
myData=read.table("finalByFrame.csv", sep=",", header=T, as.is=T)

head(myData[which(myData[,"month"]==month & myData[,"day"]==day & myData[,"station"]==station),1:14])

outputVec=c("month", "day", "period", "view", "station", "fgeno", "mgeno", "fcol", "mcol", "nf", "nm", "frame", "sex", "x", "y")


processReadme=function(myReadme){
  date=myReadme[2]
  date=unlist(strsplit(date, "_"))
  fgeno=toupper(myReadme[5])
  mgeno=toupper(myReadme[9])
  fcol=tolower(myReadme[7])
  fcol=substring(fcol, 1, 1)
  mcol=tolower(myReadme[11])
  mcol=substring(mcol, 1, 1)
  nf=as.numeric(myReadme[8])
  station=as.numeric(myReadme[4])
  year=as.numeric(date[1])
  month=as.numeric(date[2])
  day=as.numeric(date[3])
  readmeData=data.frame(month, day, station, fgeno, mgeno, fcol, mcol, nf)  
  
  return(readmeData)
}

hourlyRates=function(currentFile, readmeData, colNames){
  discRepant=c()
  periodPieces=c("am1", "am2", "pm1", "pm2")
  splitWad=strsplit(currentFile, "/")
  period=splitWad[[1]][1]
  period=which(periodPieces==period) #1:4
  viewInfo=splitWad[[1]][2]
  viewNumber=strsplit(viewInfo, "_")[[1]][2]
  viewNumber=as.numeric(substring(viewNumber,5, 5))
  classesvec=c(rep("integer", 6), rep("character", 4), rep("integer", 5), rep("double", 2))
  fileData=read.table(currentFile, sep=",", header=T)
  if(fileData[1,1] != "dummy"){
    fileData=read.table(currentFile, sep=",", colClasses=classesvec, header=T)
    colnames(fileData)=colNames
    viewData=data.frame(period=period, view=viewNumber)
    checkSumsNames=c("month", "day", "station", "fgeno", "mgeno", "fcol", "mcol", "nf")
    checkMetaNames=c("period","view")
    checksums=fileData[1,checkSumsNames]
    checkMeta=fileData[1,checkMetaNames]
    
    if(sum(checksums != readmeData) > 0 | sum(checkMeta != viewData) > 0){
      discRepant=fileData[1,]
      fileData[,checkSumsNames]=readmeData[checkSumsNames]
      fileData[,checkMetaNames]=viewData[checkMetaNames]
      fileData[,"nm"]=20-fileData[,"nf"]
      discRepant=rbind(discRepant, fileData[1,])
      discRepant[2,1]=2
      
    }
    write.table(fileData, currentFile, sep=",", row.names=F, quote=F)
  }
  return(discRepant)
}


deviations=c()
firstDir="/media/brad/Data/Documents/VideoAnalysis/stillFrames/stillAGit/clickitOut/station_2"
setwd(firstDir)
dayFolders=list.dirs(path = ".", recursive = F)

setwd(firstDir)

colNames=c("dummy", "month", "day", "period", "view", "station", "fgeno", "mgeno", "fcol", "mcol", "nf", "nm", "frame", "something", "sex", "x", "y")

for(i in 1:length(dayFolders)){
  setwd(dayFolders[i])
  myFileList=list.files(recursive=F)
  myFileList=grep("readme", myFileList, value=T)
  myReadme=scan(myFileList[1], sep=" ", what=character())
  readmeData=processReadme(myReadme)
  #head(myData[which(myData[,"month"]==month & myData[,"day"]==day & myData[,"station"]==station),1:14])
  outputFiles=list.files(path=".", recursive=T)
  outputFiles=grep("View", outputFiles, value=T)
  outputFiles=grep("csv", outputFiles, value=T)
  discRepancy=do.call(rbind, lapply(1:(length(outputFiles)), function(k) {hourlyRates(currentFile=outputFiles[k], readmeData, colNames)}))
  deviations=rbind(deviations, discRepancy)
  setwd(firstDir)
}

