hanDy=read.table("hand_curated_data.csv", sep=",", as.is=T, header=T)

han0=data.frame(index=hanDy$index, rep=hanDy$rep, month=hanDy$month, day=hanDy$day, period=hanDy$period, view=0, station=hanDy$station, genf=hanDy$genf, genm=hanDy$genm,
                colorf=hanDy$colorf, colorm=hanDy$colorm, nf=hanDy$nf, frame=hanDy$frame, nFemales=hanDy$p0f, nMales=hanDy$p0m, nUnknown=0)
han1=data.frame(index=hanDy$index, rep=hanDy$rep, month=hanDy$month, day=hanDy$day, period=hanDy$period, view=1, station=hanDy$station, genf=hanDy$genf, genm=hanDy$genm,
                colorf=hanDy$colorf, colorm=hanDy$colorm, nf=hanDy$nf, frame=hanDy$frame, nFemales=hanDy$p1f, nMales=hanDy$p1m, nUnknown=0)
han2=data.frame(index=hanDy$index, rep=hanDy$rep, month=hanDy$month, day=hanDy$day, period=hanDy$period, view=2, station=hanDy$station, genf=hanDy$genf, genm=hanDy$genm,
                colorf=hanDy$colorf, colorm=hanDy$colorm, nf=hanDy$nf, frame=hanDy$frame, nFemales=hanDy$p2f, nMales=hanDy$p2m, nUnknown=0)
han3=data.frame(index=hanDy$index, rep=hanDy$rep, month=hanDy$month, day=hanDy$day, period=hanDy$period, view=3, station=hanDy$station, genf=hanDy$genf, genm=hanDy$genm,
                colorf=hanDy$colorf, colorm=hanDy$colorm, nf=hanDy$nf, frame=hanDy$frame, nFemales=hanDy$p3f, nMales=hanDy$p3m, nUnknown=0)

newOld=rbind(han0, han1, han2, han3)
newOld=newOld[order(newOld[,"rep"]),]

newRestruct=reStruct(newOld)
write.table(newOld, "handCompiled.csv", sep=",", row.names=F)
write.table(newRestruct, "hand_curated.csv", sep=",", row.names=F)
