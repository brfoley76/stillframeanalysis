There are 3 folders, corresponding to the 3 stations downstairs. The folders contain the clickitID output from these stations.
	Each day is in a separate folder, names by the date the videos were taken
		The dates are written as yy_mm_dd
	Within each day folder, the video outputs are sorted by time period
	Each day (ideally) has 4 periods: am1 ~830-930; am2 ~930-1030; pm1 ~1800-1900; pm2 ~1900-2000.
		these are coded as periods 1:4 in the output files
	Within each day folder, there is a readme with information about date, male and female genotypes, the colors they are painted, and the sex ratio
	Note: where there are discrepancies between the data files, the readme, and evidently with the videos, I attempt to figure 
		out what the problem is, fix the readme, and adjust the .csv files to fit the readme, using the R script checkData.r
	Each day/period/ folder should have 4 csv files, corresponding to the 4 views of the cameras 0:3
	These are the outputs from Mohammad's clickitID program.
	Each line is a different fly.
	NOTE: For all of the station 3 clickit data, I'm 83% sure that the male and female id numbers were reversed. (fuck) This needs to be verified.
	
After processing with checkData.r, these files should have the following headers:
	dummy: a placeholder that we'll use to create a rep column. I don't know why we used this here?
	month: the month the video was taken
	day: the day the video was taken
	period: see above. this value should be in the range 1:4
	view: see above. This value should be in the range 0:3 (and should agree with the file name, otherwise something is wrong)
	station: see above. Should be one of 1, 2, or 3 (there is also station 0, but Sarah is using this)
	fgeno: female genotype. for this experiment, one of A, B, or C
	mgeno: male genotype. For this experiment, one of E, F, or G. if it says "FALSE", there was a glitch in checkData.r, but it should be F
	fcol: female color. one of blue (b), red (r), yellow(y), or green (g). Hopefully it's never a p, if so that probably means pink == red
	mcol: male color, see above.
	nf: number of females total in the experiment. This will be one of 5, 10 or 15. 
		NOTE: It might turn out that this is off (a couple times an extra 1 or even 2 females or males got into the experiment. We'll adjust later)
	nm: number of males total. Will sum with nf to 20. See NOTE above.
	frame: should range between 1:107001, in increments of 1000. If there are no flies on a patch for a given frame, the frame was not recorded 
		NOTE: this is annoying. but whatever.
		NOTE: How do I handle empty files (no one ever joind or left a patch??? I think I just delete it?
	something: a column which seems to told by an idiot, full of sound and fury, signifying nothing.
	sex: sex of the fly. 2==unknown. I believe 3 is female and 1 is male, but we need to double check this.
	x: the x coordinate of the fly
	y: the y coordinate of the fly

There is another folder, called "emergency repository". Ignore it. I used this as backup when I was editing files so that I didn't overwrite good data with bad.


There are 2 R files: 
	processClickit.r
		goes through all the clickIt output, and compiles it into a single file.
	checkData.r
		went through all the readme files for all the stations, and flagged and fixed most discrepancies between the output files and the readmes
		
		
Six data files are all essentially the same data, in two formats. 
	One is frame-by-frame, all patches simultaneously, with overall group summary statistics (finalByFrame, clickitByFrame and handByFrame). This is the file I'll tend to use.
	The other has each view as a separate row (finalByView, handByView, clickitByView).
	The difference is simply that the clickit data was obtained via clickit. The hand data was done first---by hand.
	
Note that these datasets *don't* contain xy coordinate information. This would be super cool -> and can easily be pulled out of the clickit files by modifying the processClickit.r code.
	
#################################
xxxByFrame.csv
	index=unique row id
	rep=unique id for a given hour within experiment
	month
	day
	station=recording station 1-3
	frame=variously 0:108000 or 1:107999; recorded every 1000 frames (should prob fix that)
	period=1-4: am1, am2, pm1, pm2
	genf=female genotype
	colorf=female colour
	genm=male genotype
	colorm=male colour
	nf=number of females 5,10,15 (totflies=20)
	p[%N]f=number of females on patch %N
	p[%N]m=number of males on patch %N
	sump=sum of all flies across all patches
	sumf=sum of females across all patches
	summ=sum of males across all patches 	
	[m/f]per[m/f]=the average number of males/females each male/female experiences in their group
	d[%N][m/f]=the change in the number of males/females on patch %N between the current frame and the next
##################################	
	
#################################
xxxByView.csv
	index=unique row id
	rep=unique id for a given hour within experiment
	month
	day
	period=1-4: am1, am2, pm1, pm2
	view=0-3
	station=recording station 1-3
	genf=female genotype
	genm=male genotype
	colorf=female colour
	colorm=male colour
	nf=number of females 5,10,15 (totflies=20)	
	frame=variously 0:108000 or 1:107999; recorded every 1000 frames (should prob fix that)
	nFemales=number of females on patch
	nMales=number of males on patch
	nUnknowns=number of individuals we don't know. Note: I've actually zeroed these out in the processClickIt.r script,
		with the patchUnknown() function. This calls out to a minimiseDelta() function that assigns sex based on whatever results 
		in the least amount of moving around; and otherwise female by default.
##################################	
	
