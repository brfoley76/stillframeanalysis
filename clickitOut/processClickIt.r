#NOTE: There is a problem with the station_3 files. Male and female identities are reversed
#ALSO NOTE: There is a small problem with counts. Occasionally there will be an extra fly in some trials.
setwd("/media/brad/Data/Documents/VideoAnalysis/stillFrames/stillframeanalysis/clickitOut/station_3")

nameVec=c("one", "month", "day", "period", "view", "station","genf", "genm", "colorf", "colorm", "nf", "nm", "frame", "otherOne", "patchFly", "flyX", "flyY")
frameVec=c(0:107)
frameVec=frameVec*1000
frameVec=frameVec+1

reStruct=function(myCompiled){
  p0Vec=c(myCompiled[,"view"]==0)
  p1Vec=c(myCompiled[,"view"]==1)
  p2Vec=c(myCompiled[,"view"]==2)
  p3Vec=c(myCompiled[,"view"]==3)
  
  myStruct=myCompiled[p0Vec,]
  p0m=myCompiled[p0Vec,"nMales"]
  p0f=myCompiled[p0Vec,"nFemales"]
  p1m=myCompiled[p1Vec,"nMales"]
  p1f=myCompiled[p1Vec,"nFemales"]
  p2m=myCompiled[p2Vec,"nMales"]
  p2f=myCompiled[p2Vec,"nFemales"]
  p3m=myCompiled[p3Vec,"nMales"]
  p3f=myCompiled[p3Vec,"nFemales"]
  
  sump=c(p0f+ p0m+p1f+p1m+p2f+p2m+p3f+p3m)
  sumf=c( p0f+p1f+p2f+p3f)
  summ=c(p0m+p1m+p2m+p3m)
  mperm=(p0m*(p0m-1)+p1m*(p1m-1)+p2m*(p2m-1)+p3m*(p3m-1))/summ
  mperm[is.na(mperm)]=0
  fperm=(p0m*(p0f)+p1m*(p1f)+p2m*(p2f)+p3m*(p3f))/summ
  fperm[is.na(fperm)]=0
  mperf=(p0m*(p0f)+p1m*(p1f)+p2m*(p2f)+p3m*(p3f))/sumf
  mperf[is.na(mperf)]=0
  fperf=(p0f*(p0f-1)+p1f*(p1f-1)+p2f*(p2f-1)+p3f*(p3f-1))/sumf
  fperf[is.na(fperf)]=0
  
  lengthMinus1=length(p0m)-1
  lengthLength=length(p0m)
  d0m=p0m[2:lengthLength]-p0m[1:lengthMinus1]
  d0m=c(d0m, NA)
  d1m=p1m[2:lengthLength]-p1m[1:lengthMinus1]
  d1m=c(d1m, NA)
  d2m=p2m[2:lengthLength]-p2m[1:lengthMinus1]
  d2m=c(d2m, NA)
  d3m=p3m[2:lengthLength]-p3m[1:lengthMinus1]
  d3m=c(d3m, NA)
  
  d0f=p0f[2:lengthLength]-p0f[1:lengthMinus1]
  d0f=c(d0f, NA)
  d1f=p1f[2:lengthLength]-p1f[1:lengthMinus1]
  d1f=c(d1f, NA)
  d2f=p2f[2:lengthLength]-p2f[1:lengthMinus1]
  d2f=c(d2f, NA)
  d3f=p3f[2:lengthLength]-p3f[1:lengthMinus1]
  d3f=c(d3f, NA)  #Fuck. This only works for the last entry. I need to make sure to NA all the final entries in each 2 hour block.

  myRestruct=data.frame(myStruct[,c("index", "rep", "month", "day", "station", "frame", "period", "genf", "colorf", "genm", "colorm", "nf")], p0f, p0m, p1f, p1m, p2f, p2m, p3f, p3m, sump, sumf, summ, mperm, fperm, mperf, fperf, d0f, d0m, d1f, d1m, d2f, d2m, d3f, d3m)
}

minimiseDelta=function(frameWindow, toChange){
  nUnknowns=toChange[,"nUnknown"]
  nCols=nUnknowns+1
  assignFrame=data.frame(0:nUnknowns, nUnknowns:0)
  tMinus1DeltaFrame=assignFrame
  tMinus1DeltaFrame=tMinus1DeltaFrame+tMinus1DeltaFrame*0.1 #we want to minorly penalise lots of one sex moving all at once
  tMinus1DeltaFrame[,2]=tMinus1DeltaFrame[,2]-0.01 #all else being equal, more likely a male than a female moved
  tPlus1DeltaFrame=tMinus1DeltaFrame
  
  tMinus1Delta=data.frame(frameWindow[2,c("nFemales", "nMales")]-frameWindow[1,c("nFemales", "nMales")])
  tMinus1Delta=tMinus1Delta[rep(1,nCols),]
  tPlus1Delta=data.frame(frameWindow[3,c("nFemales", "nMales")]-frameWindow[2,c("nFemales", "nMales")])
  tPlus1Delta=tPlus1Delta[rep(1,nCols),]
  
  tMinus1DeltaFrame=abs(tMinus1DeltaFrame[,]+tMinus1Delta)
  tPlus1DeltaFrame=abs(tPlus1DeltaFrame[,]-tPlus1Delta)
  sumTDelta=tMinus1DeltaFrame+tPlus1DeltaFrame
  summIty=rowSums(sumTDelta)
  minRow=which(summIty==min(summIty))
  toChange[,"nFemales"]=toChange[,"nFemales"]+assignFrame[minRow[1],1]
  toChange[,"nMales"]=toChange[,"nMales"]+assignFrame[minRow[1],2]
  toChange[,"nUnknown"]=0
  
  return(toChange)
}


patchUnknown=function(myCompiled){
  if(sum(myCompiled[,"nUnknown"])>0){
    unGnone=myCompiled[c(myCompiled[,"nUnknown"] >0),]
    rePlacements=do.call(rbind, lapply(1:nrow(unGnone), function(k) {minimiseDelta(frameWindow=myCompiled[(unGnone[k,1]-1):(unGnone[k,1]+1),], toChange=unGnone[k,])}))
    for(i in 1:nrow(rePlacements)){
      myCompiled[which(myCompiled[,"index"]==rePlacements[i,"index"]),]=rePlacements[i,]       
    }
  }
  return(myCompiled)
}


####For station_3 I had to reverse males and females. Usually males should be 1, females 3. 
countABilly=function(currentFile, frameNo, metaChunk){
  frameData=currentFile[currentFile[,"frame"]==frameNo,]
  countMales=sum(frameData[,"patchFly"]==3)
  countFemales=sum(frameData[,"patchFly"]==1)
  countUnknown=sum(frameData[,"patchFly"]==2)
  metaChunk["frame"]=frameNo
  
  currentLine=data.frame(metaChunk, nFemales=countFemales, nMales=countMales, nUnknown=countUnknown, stringsAsFactors=F)
  return(currentLine)
}

myCompiled=c()
myFileList=list.files(recursive=T)
grepl("csv~", myFileList)
rep=1
for(i in 1:(length(myFileList))){
  if(grepl("csv", myFileList[i])){
  currentFile=read.table(myFileList[i], header=T, as.is=T, sep=",", stringsAsFactors=F, colClasses=c(rep("integer", 6), rep("character",4), rep("integer", 5), rep("numeric", 2)))
  colnames(currentFile)=nameVec
  metaChunk=currentFile[1,c(2:11, 13)]
  myOutput=do.call(rbind, lapply(1:length(frameVec), function(k) {countABilly(currentFile, frameNo=frameVec[k], metaChunk)}))
  myOutput=data.frame(rep, myOutput)
  rep=rep+1
  myCompiled=rbind(myCompiled, myOutput)
  print(i)
  }else{
    print("I am not a looney!")
  }
}
setwd("..")
myCompiled[myCompiled[,"genm"]=="FALSE","genm"]="F"  ###Something annoying and weird here. eventually had to write out, get rid of the "FALSE", and read back in.
index=c(1:nrow(myCompiled))
myCompiled=data.frame(index, myCompiled)
myCompiled[which(is.na(myCompiled[,"nFemales"])),"nFemales"]=0
myCompiled[which(is.na(myCompiled[,"nMales"])),"nMales"]=0
myCompiled[which(is.na(myCompiled[,"nUnknown"])),"nUnknown"]=0
myCompiled=patchUnknown(myCompiled)

#here, sort out the reps, and rbind the various myCompileds (from folders 1, 2, 3) together
#write.table(myCompiled, "clickitByView.csv", sep=",", row.names=F)
#######################
###  END CHUNK 1  #####
#######################
#######################

#myCompiled2[c(T,(myCompiled2[1:(nrow(myCompiled2)-1),2]!=myCompiled2[2:nrow(myCompiled2),2])),] #quick visualisation of the first row of each rep
#myRestruct[c(T,(myRestruct[1:(nrow(myRestruct)-1),2]!=myRestruct[2:nrow(myRestruct),2])),]

#myRestruct=rbind(myRestruct, myData[,1:35])

handView=read.table("handByView.csv", sep=",", header=T, as.is=T)
clickitView=read.table("clickitByView.csv", sep=",", header=T, as.is=T)
myCompiled=rbind(clickitView, handView)
myCompiled[,"index"]=c(1:nrow(myCompiled))
#the following for loop is exasperatingly slow. But I don't quite know how to vectorise it, so fuck it.
thisrep=1
for(i in 1:nrow(myCompiled)){
  myCompiled[i,"rep"]=thisrep
  myCompiled[i,"index"]=i
  if(myCompiled[i,"frame"] >107000){
    thisrep=thisrep+1
    print(thisrep)
  }
}


write.table(myCompiled, "finalByView.csv", row.names=F, sep=",")
myRestruct=reStruct(myCompiled)

thisrep=1
for(i in 1:nrow(myRestruct)){
  myRestruct[i,"rep"]=thisrep
  myRestruct[i,"index"]=i
  if(myRestruct[i,"frame"] >107000){
    thisrep=thisrep+1
    print(thisrep)
  }
}

myData=myRestruct
myData[myData[,"genm"]=="g", "genm"]="G"
myData[myData[,"genf"]=="c", "genf"]="C"


write.table(myRestruct, "finalByFrame.csv", row.names=F, sep=",")

################################################################
################################################################
