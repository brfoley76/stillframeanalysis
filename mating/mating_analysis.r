setwd("/media/brad/Data/Documents/VideoAnalysis/stillFrames/stillAGit")
library(lme4)

#setwd("E:/Documents/VideoAnalysis/sexRatio/julia")
mating=read.table("matingdatafinal_clean.csv", sep=",", as.is=T, header=T, strip.white=T)


mperm=(mating$p0m*(mating$p0m-1)+mating$p1m*(mating$p1m-1)+mating$p2m*(mating$p2m-1)+mating$p3m*(mating$p3m-1))/(mating$p0m+mating$p1m+mating$p2m+mating$p3m)
fperm=(mating$p0m*(mating$p0f)+mating$p1m*(mating$p1f)+mating$p2m*(mating$p2f)+mating$p3m*(mating$p3f))/(mating$p0m+mating$p1m+mating$p2m+mating$p3m)
mperf=(mating$p0m*(mating$p0f)+mating$p1m*(mating$p1f)+mating$p2m*(mating$p2f)+mating$p3m*(mating$p3f))/(mating$p0f+mating$p1f+mating$p2f+mating$p3f)
fperf=(mating$p0f*(mating$p0f-1)+mating$p1f*(mating$p1f-1)+mating$p2f*(mating$p2f-1)+mating$p3f*(mating$p3f-1))/(mating$p0f+mating$p1f+mating$p2f+mating$p3f)

propm=(mating$p0m+mating$p1m+mating$p2m+mating$p3m)/(20-mating$nf)
propf=(mating$p0f+mating$p1f+mating$p2f+mating$p3f)/mating$nf

mating_nmales=(mating$p0m+mating$p1m+mating$p2m+mating$p3m)/4 
mating_nfemales=(mating$p0f+mating$p1f+mating$p2f+mating$p3f)/4 

mperm[is.na(mperm)]=0
fperm[is.na(fperm)]=0
mperf[is.na(mperf)]=0
fperf[is.na(fperf)]=0

malepatchvec=c("p0m", "p1m", "p2m", "p3m")
femalepatchvec=c("p0f", "p1f", "p2f", "p3f")


for(i in 1:4){
  mating_nmales[c(mating$mating==1 & mating$patchmating==i)]=mating[c(mating$mating==1 & mating$patchmating==i),malepatchvec[i]]
  mating_nfemales[c(mating$mating==1 & mating$patchmating==i)]=mating[c(mating$mating==1 & mating$patchmating==i),femalepatchvec[i]]
}
###Let's restructure it.
### Dammit - also, I'm manually going to go through and remove the extra measures 
### from where there are mating events in a given period. 
### This complicated enough that I should do it by hand.
### all periods should be represented with 4 timepoints (otherwise, we're systematically overestimating the "not mating" timepoints)
### where possible, the deleted timepoints should be *immediately after* the mating events, because patch counts are a little ambiguous
### when some of the flies are mating
matingVec=mating[,"mating"]!=1
postMating=c(TRUE, matingVec[1:(length(matingVec)-1)])
mating=data.frame(mating, postMating)

write.table(mating, "matingdatafinal_processed.csv", sep=",", quote=T)
mating=read.table("matingdatafinal_processed.csv", sep=",", header=T, as.is=T)
matingStruct=c()

namesMating=colnames(mating)
namesMating=namesMating[1:13]
namesMating=c(namesMating, "npF", "npM", "mating", "patch", "postMating")
for(i in 0:3){
  matingChunk=mating[,c(1:13, (14+i*2), (15+i*2))]
  patchMating=mating[,"patchmating"]
  patchMating[patchMating!=i]=9 #don't forget about patch 0. 
  patchMating[patchMating==i]=1
  patchMating[patchMating==9]=0 
  matingChunk=cbind(matingChunk, patchMating, i, mating[,"postMating"])  
  colnames(matingChunk)=namesMating
  matingStruct=rbind(matingStruct, matingChunk)
}
fPerM=matingStruct[,"npF"]/matingStruct[,"npM"]
mPerF=matingStruct[,"npM"]/matingStruct[,"npF"]
fPerM[is.na(fPerM)]=0
mPerF[is.na(mPerF)]=0
fPerM[fPerM==Inf]=0
mPerF[mPerF==Inf]=0
matingStruct=data.frame(matingStruct, fPerM, mPerF)

matingStructMating=matingStruct[matingStruct[,"mating"]==1,]
mWeights=matingStruct[,"npM"]-matingStruct[,"mating"]
fWeights=matingStruct[,"npF"]-matingStruct[,"mating"]
matingStruct=data.frame(matingStruct, mWeights, fWeights)
matingStruct[,"mating"]=0
matingStructMating=data.frame(matingStructMating, mWeights=1, fWeights=1)
matingStruct=rbind(matingStruct, matingStructMating)

matingStruct=matingStruct[!is.na(matingStruct[,1]),]
matingStruct=matingStruct[!is.na(matingStruct[,"npF"]),]

write.table(matingStruct, "matingRestructured.csv", sep=",", row.names=F, quote=F)

#####And now we can do logistic regression!
aaa=glmer(mating~period + nf +(npM|genom)+fPerM, data=matingStruct[c(matingStruct[,"npF"]>0 & matingStruct[,"npM"]>0),], weights=mWeights, family=binomial)
relgrad=with(aaa@optinfo$derivs, solve(Hessian, gradient))
max(abs(relgrad))

bbb=glm(mating~period + nf + fPerM + npM, data=matingStruct[c(matingStruct[,"npF"]>0 & matingStruct[,"npM"]>0),], weights=mWeights, family=binomial)
ccc=glm(mating~period + nf + npM + npF, data=matingStruct[c(matingStruct[,"npF"]>0 & matingStruct[,"npM"]>0),], weights=fWeights, family=binomial)
ddd=glm(mating~period + nf + npM + npF, data=matingStruct[c(matingStruct[,"npF"]>0 & matingStruct[,"npM"]>0),], weights=mWeights, family=binomial)
eee=glm(mating~period + nf + genom+fPerM + npM + npF, data=matingStruct[c(matingStruct[,"npF"]>0 & matingStruct[,"npM"]>0),], weights=mWeights, family=binomial)


mating=data.frame(mating, mperm, fperm, mperf, fperf, propm, propf, mating_nmales, mating_nfemales)

boxplot(mating$mperf~mating$genom+mating$nf)

boxplot(mating$fperm~mating$genom+mating$nf)
boxplot(aaa$x~aaa$Group.2+aaa$Group.3)
aaa=glmer(mating~period+(nf|genom), data=mating, family=binomial)
coef(aaa)

aaa=glm(mating~mating_nmales+mating_nfemales+nf, data=mating, family=binomial(logit))
aaa=glm((mperm/(20-nf))~genom+genof+nf, data=mating)


par(mfrow=c(1,2))
jpeg("rplot.jpg", width=900, height=450)
par(mfrow=c(1,2))
plot(propm~nf, data=mating, cex=2)
aaa=lm(propm~nf, data=mating)
abline(aaa)

plot(propf~nf, data=mating, cex=2)
bbb=lm(propf~nf, data=mating)
abline(bbb)
summary(bbb)
dev.off()

aaa=glm(mating~mating_nfemales, data=mating, family=binomial())

b=-0.0868
m=0.1266
x=c(5, 10, 15)
y=m*x+b





