#########################################################################
#### read in the various datasets: the mating data, the compiled frame data
#### and the "readme" data, and make sure they agree. And if not, why.


setwd("/media/brad/Data/Documents/VideoAnalysis/stillFrames/stillAGit")

allTrials=read.csv("myCompiled1.csv", header=T, as.is=T)
mating=read.csv("matingdatafinal.csv", header=T, as.is=T)
myData=read.csv("provisionalCompiled.csv", header=T, as.is=T)
myData=data.frame(myData, count=1)

matingReps=aggregate(mating[,"mating"], by=list(mating[,"rep"], mating[,"month"], mating[,"day"], mating[,"station"], mating[,"nf"], mating[,"genof"], mating[,"genom"]), FUN="sum")
names(matingReps)=c("rep", "month", "day", "station", "nf", "fgeno", "mgeno", "mating")
myDataCheck=myData[,c("month", "day", "station", "genf", "genm", "nf", "count")]
colnames(myDataCheck)=c("month", "day", "station", "fgeno", "mgeno", "nf", "count")
myDataCheck=aggregate(myDataCheck[,"count"], by=list(myDataCheck[,"month"], myDataCheck[,"day"], myDataCheck[,"station"], myDataCheck[,"fgeno"], myDataCheck[,"mgeno"], myDataCheck[,"nf"]), FUN="sum")
colnames(myDataCheck)=c("month", "day", "station", "fgeno", "mgeno", "nf", "count")

matingReps=data.frame(matingReps, mateScore=1)
allTrials=data.frame(allTrials, readme=1)
myDataCheck=data.frame(myDataCheck, clickit=1)
checkalign=merge(allTrials, matingReps, by=c("month", "day", "station", "nf", "fgeno", "mgeno"), all=T, incomparables="NA")
checkalign=merge(checkalign, myDataCheck, by=c("month", "day", "station", "nf", "fgeno", "mgeno"), all=T, incomparables="NA")

checkalign=checkalign[order(checkalign[,"day"]),]
checkalign=checkalign[order(checkalign[,"month"]),]

write.csv(checkalign, "checkalign.csv", row.names=F, quote=F)
