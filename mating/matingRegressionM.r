setwd("/media/brad/Data/Documents/VideoAnalysis/stillFrames/stillAGit")
matingStruct=read.table("matingRestructured.csv", sep=",", header=T, as.is=T)
library(lme4)


makeColumn=function(k, numRows){
  repLength=numRows/(2^(k))  
  oneVec=rep(1, repLength)
  zeroVec=rep(0, repLength)
  repVec=c(zeroVec, oneVec)
  colVec=rep(repVec, numRows/(repLength*2))
  return(colVec)
}

makeFactorialMatrix=function(lengthVec){
  numRows=2^lengthVec
  factMat=do.call(cbind, lapply(1:lengthVec, function(k) {makeColumn(k, numRows)}))
}

#####Looks non-terrible, though the data is fucked six ways from Sunday
####Next, model selection and plots.

#First create the two combinations matrices (one with random effects, and one without)
mVarVec=c("nf", "npF", "npM", "npF:npM", "fPerM", "(1 | genof)", "(npF | genof)", "(npF | genom)", "(1 | genom)", "(npM | genom)", "period")
allCombosM=makeFactorialMatrix(length(mVarVec))
colnames(allCombosM)=mVarVec

allCombosM=allCombosM[c(allCombosM[,"period"]==1),]
#allCombosF=allCombosF[c(allCombosF[,"nf"]==1),]
zzz=rowSums(allCombosM[,6:10])
NoRandom=allCombosM[zzz==0,]

zzz=rowSums(allCombosM[,c(6:10)])  ###at least one random effect
zzz=zzz>0
allCombosM=allCombosM[zzz,]

zzz=rowSums(allCombosM[,c(2:3)]) ###if we have the pf:pm interaction term, we need to have the main pf and pm terms
yyy=allCombosM[,4]
xxx=c(zzz<2 & yyy==1)
xxx= !xxx
allCombosM=allCombosM[xxx,]

zzz=rowSums(allCombosM[,c(6:7)])
zzz=zzz<2
allCombosM=allCombosM[zzz,]

zzz=rowSums(allCombosM[,c(8:10)])
zzz=zzz<3
allCombosM=allCombosM[zzz,]

modelInfo=c()
#FMOD "fmodx=glmer(leave~nf + pf + pm + pf:pm +(pf | genf) + (1+ pm | genm) + period, data=mDatExp, family=\"binomial")"
mFormVec=c("nf + ", "npF + ", "npM + ", "npF:npM + ", "fPerM + ", "(1 | genof) + ", "(npF | genof) + ", "(npF | genom) + ", "(1 | genom) + ", "(npM | genom) + ", "period")

####The following two are the models containing random effects
for(i in 1:nrow(allCombosM)){
  modName=paste("mmod", i, sep="")
  modTextVec=mFormVec[allCombosM[i,]==1]
  cat(modName, "=glmer(mating~", file="modEncodeM.r", sep="")
  cat(modTextVec, file="modEncodeM.r", sep="", append=T)
  cat(", data=matingStruct[c(matingStruct[, \"npM\"] > 0 & matingStruct[,\"postMating\"]), ], weights = mWeights, family=\"binomial\")\nmodFitLine=c(modName, summary(", modName,")$AICtab)", file="modEncodeM.r", sep="", append=T)
  source("modEncodeM.r")
  cat(modName, modTextVec, modFitLine, "\n", file="resultsOfMatingMModelSelect.txt", append=T, sep=",")
}


for(i in 1:nrow(allCombosM)){
  modName=paste("mmod", i, sep="")
  modTextVec=mFormVec[allCombosM[i,]==1]
  cat(modName, "=glmer(mating~", file="modEncodeM.r", sep="")
  cat(modTextVec, file="modEncodeM.r", sep="", append=T)
  cat(", data=matingStruct[c(matingStruct[, \"npM\"] > 0 & matingStruct[,\"npF\"]>0 & matingStruct[,\"postMating\"]), ], weights = mWeights, family=\"binomial\")\nmodFitLine=c(modName, summary(", modName,")$AICtab)", file="modEncodeM.r", sep="", append=T)
  source("modEncodeM.r")
  cat(modName, modTextVec, modFitLine, "\n", file="resultsOfMatingMModelSelect_mfmorethan1.txt", append=T, sep=",")
}
#### the following loops don't have random effects.

for(i in 1:nrow(NoRandom)){
  modName=paste("mmod", i, sep="")
  modTextVec=mFormVec[NoRandom[i,]==1]
  cat(modName, "=glm(mating~", file="modEncodeM.r", sep="")
  cat(modTextVec, file="modEncodeM.r", sep="", append=T)
  cat(", data=matingStruct[c(matingStruct[, \"npM\"] > 0 & matingStruct[,\"postMating\"]), ]", file="modEncodeM.r", sep="", append=T)
  cat(", weights = mWeights, family=\"binomial\")", file="modEncodeM.r", sep="", append=T)
  cat("\nmodFitLine=c(modName, AIC=AIC(", modName,"), BIC=BIC(", modName,"), logLik=as.numeric(logLik(",modName,")),", file="modEncodeM.r", sep="", append=T)
  cat("deviance=summary(",modName,")$deviance, df.residual=summary(",modName,")$df.residual)",file="modEncodeM.r", sep="", append=T) 

  source("modEncodeM.r")
  cat(modName, modTextVec, modFitLine, "\n", file="resultsOfMatingMModelSelect.txt", append=T, sep=",")
}


for(i in 1:nrow(NoRandom)){
  modName=paste("mmod", i, sep="")
  modTextVec=mFormVec[NoRandom[i,]==1]
  cat(modName, "=glm(mating~", file="modEncodeM.r", sep="")
  cat(modTextVec, file="modEncodeM.r", sep="", append=T)
  cat(", data=matingStruct[c(matingStruct[, \"npM\"] > 0 & matingStruct[,\"npF\"]>0 & matingStruct[,\"postMating\"]), ]", file="modEncodeM.r", sep="", append=T)
  cat(", weights = mWeights, family=\"binomial\")", file="modEncodeM.r", sep="", append=T)
  cat("\nmodFitLine=c(modName, AIC=AIC(", modName,"), BIC=BIC(", modName,"), logLik=as.numeric(logLik(",modName,")),", file="modEncodeM.r", sep="", append=T)
  cat("deviance=summary(",modName,")$deviance, df.residual=summary(",modName,")$df.residual)",file="modEncodeM.r", sep="", append=T) 
  
  source("modEncodeM.r")
  cat(modName, modTextVec, modFitLine, "\n", file="resultsOfMatingMModelSelect_mfmorethan1.txt", append=T, sep=",")
}

#####Sorry, you need to clean up the extra commas in the .csv file before you can use it.
####### just replace " ," with " "
myMModResults=read.csv("resultsOfMatingMModelSelect.txt", header=F)
myMModResults=myMModResults[,c(1, 2, 4:8)]
colnames(myMModResults)=c("modName", "model", "AIC", "BIC", "logLik", "deviance", "dfResid")

myMModsRes=myMModResults[order(myMModResults[,"BIC"]),]
head(myMModsRes, 10)

myMModResults1=read.csv("resultsOfMatingMModelSelect_mfmorethan1.txt", header=F)
myMModResults1=myMModResults1[,c(1, 2, 4:8)]
colnames(myMModResults1)=c("modName", "model", "AIC", "BIC", "logLik", "deviance", "dfResid")

myMModsRes1=myMModResults1[order(myMModResults1[,"BIC"]),]
head(myMModsRes1, 10)

################Here 
lll=glmer(mating~nf + npF + (1 | genof) + period, data=matingStruct[c(matingStruct[, "npM"] > 0 ), ], weights = mWeights, family="binomial")
mmm=glm(mating~nf + npF + genof + period, data=matingStruct[c(matingStruct[, "npM"] > 0) ,] , weights = mWeights, family="binomial")
jjj=glmer(mating~npF + npM + (1 | genom) + period, data=matingStruct[c(matingStruct[, "npM"] > 0 ), ], weights = mWeights, family="binomial")
kkk=glm(mating~npF + npM + genom + period, data=matingStruct[c(matingStruct[, "npM"] > 0) ,] , weights = mWeights, family="binomial")
coef(lll)
ranef(lll)
anova(mmm, test="Chisq")



nnn=glmer(mating~npF + npM + (1 | genof) + period, data=matingStruct[c(matingStruct[, "npM"] > 0 & matingStruct[, "npF"] > 0), ], weights = mWeights, family="binomial")
ooo=glmer(mating~nf + (1 | genof) + period , data=matingStruct[c(matingStruct[, "npM"] > 0 & matingStruct[, "npF"] > 0), ], weights = mWeights, family="binomial")
ppp=glm(mating~npF + npM + genof + period, data=matingStruct[c(matingStruct[, "npM"] > 0 & matingStruct[, "npF"] > 0), ], weights = mWeights, family="binomial")
qqq=glm(mating~nf + genof + period, data=matingStruct[c(matingStruct[, "npM"] > 0 & matingStruct[, "npF"] > 0), ], weights = mWeights, family="binomial")

mDat=read.table("mDat.csv", sep=",", as.is=T, header=T)
zzz=glmer(leave~period + nf + fPerM + (1|genf) + (pm|genm), data=mDat, weights=weight, family="binomial")


###########################################################
explodeDat=function(myDat){
  expDat=myDat[rep(1,myDat[,"mWeights"]),]
  return(expDat)  
}

mMateDat=matingStruct[c(matingStruct[, "npM"] > 0 ),]
mDatExp=do.call(rbind, lapply(1:nrow(mMateDat), function(k) {explodeDat(mMateDat[k,])}))
qqq=MCMChlogit(fixed=mating~npF + npM + period , random=~npF, group="genof", 
               burnin=5000, mcmc=1000, thin=1,verbose=1,
               seed=NA, beta.start=0, sigma2.start=1,
               Vb.start=1, mubeta=0, Vbeta=1.0E6,
              data=mDatExp,r=2, R=diag(c(0.1, 0.1)), nu=0.001, delta=0.001, FixOD=1)

