setwd("/media/brad/Data/Documents/VideoAnalysis/stillFrames/stillAGit")
matingStruct=read.table("matingRestructured.csv", sep=",", header=T, as.is=T)

makeColumn=function(k, numRows){
  repLength=numRows/(2^(k))  
  oneVec=rep(1, repLength)
  zeroVec=rep(0, repLength)
  repVec=c(zeroVec, oneVec)
  colVec=rep(repVec, numRows/(repLength*2))
  return(colVec)
}

makeFactorialMatrix=function(lengthVec){
  numRows=2^lengthVec
  factMat=do.call(cbind, lapply(1:lengthVec, function(k) {makeColumn(k, numRows)}))
}

#####Looks non-terrible, though the data is fucked six ways from Sunday
####Next, model selection and plots.

fVarVec=c("nf", "npF", "npM", "npF:npM", "mPerF", "(1 | genof)", "(npF | genof)", "(npM | genof)", "(1 | genom)", "(npM | genom)", "period")
allCombosF=makeFactorialMatrix(length(fVarVec))
colnames(allCombosF)=fVarVec

allCombosF=allCombosF[c(allCombosF[,"period"]==1),]
#allCombosF=allCombosF[c(allCombosF[,"nf"]==1),]
zzz=rowSums(allCombosF[,6:10])
NoRandom=allCombosF[zzz==0,]

zzz=rowSums(allCombosF[,c(6:10)])  ###at least one random effect
zzz=zzz>0
allCombosF=allCombosF[zzz,]

zzz=rowSums(allCombosF[,c(2:3)]) ###if we have the pf:pm interaction term, we need to have the main pf and pm terms
yyy=allCombosF[,4]
xxx=c(zzz<2 & yyy==1)
xxx= !xxx
allCombosF=allCombosF[xxx,]

zzz=rowSums(allCombosF[,c(6:8)])
zzz=zzz<3
allCombosF=allCombosF[zzz,]

zzz=rowSums(allCombosF[,c(9:10)])
zzz=zzz<2
allCombosF=allCombosF[zzz,]

modelInfo=c()
#FMOD "fmodx=glmer(leave~nf + pf + pm + pf:pm +(pf | genf) + (1+ pm | genm) + period, data=mDatExp, family=\"binomial")"
fFormVec=c("nf + ", "npF + ", "npM + ", "npF:npM + ", "mPerF + ", "(1 | genof) + ", "(npF | genof) + ", "(npM | genof) + ", "(1 | genom) + ", "(npM | genom) + ", "period")

for(i in 1:nrow(allCombosF)){
  modName=paste("fmod", i, sep="")
  modTextVec=fFormVec[allCombosF[i,]==1]
  cat(modName, "=glmer(mating~", file="modEncodeF.r", sep="")
  cat(modTextVec, file="modEncodeF.r", sep="", append=T)
  cat(", data=matingStruct[c(matingStruct[, \"npF\"] > 0  & matingStruct[,\"postMating\"]), ], weights = fWeights, family=\"binomial\")\nmodFitLine=c(modName, summary(", modName,")$AICtab)", file="modEncodeF.r", sep="", append=T)
  source("modEncodeF.r")
  cat(modName, modTextVec, modFitLine, "\n", file="resultsOfMatingModelSelF.txt", append=T, sep=",")
}


for(i in 1:nrow(allCombosF)){
  modName=paste("fmod", i, sep="")
  modTextVec=fFormVec[allCombosF[i,]==1]
  cat(modName, "=glmer(mating~", file="modEncodeF.r", sep="")
  cat(modTextVec, file="modEncodeF.r", sep="", append=T)
  cat(", data=matingStruct[c(matingStruct[, \"npF\"] > 0 & matingStruct[,\"npM\"]>0 & matingStruct[,\"postMating\"]), ], weights = fWeights, family=\"binomial\")\nmodFitLine=c(modName, summary(", modName,")$AICtab)", file="modEncodeF.r", sep="", append=T)
  source("modEncodeF.r")
  cat(modName, modTextVec, modFitLine, "\n", file="resultsOfMatingModelSelF_mfmorethan1.txt", append=T, sep=",")
}

#### the following loops don't have random effects.
for(i in 1:nrow(NoRandom)){
  modName=paste("fmod", i, sep="")
  modTextVec=fFormVec[NoRandom[i,]==1]
  cat(modName, "=glm(mating~", file="modEncodeF.r", sep="")
  cat(modTextVec, file="modEncodeF.r", sep="", append=T)
  cat(", data=matingStruct[c(matingStruct[, \"npF\"] > 0 & matingStruct[,\"postMating\"]), ]", file="modEncodeF.r", sep="", append=T)
  cat(", weights = fWeights, family=\"binomial\")", file="modEncodeF.r", sep="", append=T)
  cat("\nmodFitLine=c(modName, AIC=AIC(", modName,"), BIC=BIC(", modName,"), logLik=as.numeric(logLik(",modName,")),", file="modEncodeF.r", sep="", append=T)
  cat("deviance=summary(",modName,")$deviance, df.residual=summary(",modName,")$df.residual)",file="modEncodeF.r", sep="", append=T) 
  
  source("modEncodeF.r")
  cat(modName, modTextVec, modFitLine, "\n", file="resultsOfMatingFModelSelect.txt", append=T, sep=",")
}


for(i in 1:nrow(NoRandom)){
  modName=paste("fmod", i, sep="")
  modTextVec=fFormVec[NoRandom[i,]==1]
  cat(modName, "=glm(mating~", file="modEncodeF.r", sep="")
  cat(modTextVec, file="modEncodeF.r", sep="", append=T)
  cat(", data=matingStruct[c(matingStruct[, \"npM\"] > 0 & matingStruct[,\"npF\"]>0 & matingStruct[,\"postMating\"]), ]", file="modEncodeF.r", sep="", append=T)
  cat(", weights = fWeights, family=\"binomial\")", file="modEncodeF.r", sep="", append=T)
  cat("\nmodFitLine=c(modName, AIC=AIC(", modName,"), BIC=BIC(", modName,"), logLik=as.numeric(logLik(",modName,")),", file="modEncodeF.r", sep="", append=T)
  cat("deviance=summary(",modName,")$deviance, df.residual=summary(",modName,")$df.residual)",file="modEncodeF.r", sep="", append=T) 
  
  source("modEncodeF.r")
  cat(modName, modTextVec, modFitLine, "\n", file="resultsOfMatingFModelSelect_mfmorethan1.txt", append=T, sep=",")
}


















myFModResults1=read.csv("resultsOfMatingModelSelF_mfmorethan1.txt", header=F)
myFModResults1=myFModResults1[,c(1, 2, 4:8)]
colnames(myFModResults1)=c("modName", "model", "AIC", "BIC", "logLik", "deviance", "dfResid")

myFModsRes1=myFModResults1[order(myFModResults1[,"BIC"]),]
head(myFModsRes1, 10)

aaa=glmer(mating~(1 | genof) + period, data=matingStruct[c(matingStruct[, "npF"] > 0 & matingStruct[, "npM"] > 0 & matingStruct[,"postMating"]), ], weights = fWeights, family="binomial")
bbb=glm(mating~genof + period, data=matingStruct[c(matingStruct[, "npF"] > 0 & matingStruct[, "npM"] > 0 & matingStruct[,"postMating"]), ], weights = fWeights, family="binomial")
zzz=glm(mating~genom  + mPerF+ period, data=matingStruct[c(matingStruct[, "npF"] > 0 & matingStruct[, "npM"] > 0 & matingStruct[,"postMating"]), ], weights = fWeights, family="binomial")
coef(aaa)
ranef(aaa)
anova(bbb, test="Chisq")

myFModResults=read.csv("resultsOfMatingModelSelF.txt", header=F)
myFModResults=myFModResults[,c(1, 2, 4:8)]
colnames(myFModResults)=c("modName", "model", "AIC", "BIC", "logLik", "deviance", "dfResid")

myFModsRes=myFModResults[order(myFModResults[,"BIC"]),]
head(myFModsRes, 10)

eee=glmer(mating~mPerF + (1 | genof) + period, data=matingStruct[c(matingStruct[, "npF"] > 0), ], weights = fWeights, family="binomial")
fff=glmer(mating~(1 | genof) + period , data=matingStruct[c(matingStruct[, "npF"] > 0), ], weights = fWeights, family="binomial")
ggg=glm(mating~mPerF +genof + period, data=matingStruct[c(matingStruct[, "npF"] > 0), ], weights = fWeights, family="binomial")
hhh=glm(mating~genof + period, data=matingStruct[c(matingStruct[, "npF"] > 0), ], weights = fWeights, family="binomial")

ranef(eee)
coef(eee)