This folder contains most of the analysis files, and output files that I've been playing with.
There are 4 R files: 
	analyseLeaving.r reads in the ./clickitOut/finalByFrame.csv file, and converts it to two different formats suitable for logistic regression
		output: fDat.csv and mDat.csv
	mcmcpackRegressionF.r/mcmcpackRegressionM.r does model selection for mixed model logistic regression analysis 
		of the joining and leaving data for females and males repsectively.

	playingWithData.r (see below) is a walkthrough of the analyses and results
	
There are 2 major data files (sex specific): 
########################
fDat.csv and mDat.csv
	period=time of day (am1, am2, pm1, pm2) -> (1, 2, 3, 4)
	genf=the genotype of the females in the expt.
	genm=the genotype of the males in the expt
	nf=the number of females in the expt (nm=20-nf). [Might need to change these if there were too many flies?]
	pf/pm=the number of females/males on the patch at frame t
	sex=the focal sex (this is an irrelevant variable, I only used it to subdivide the original data)
	leave=whether a fly left (1) or not (0) between frame t and frame t+1
	offF/offM=the number of flies imputed to be off patch at frame t. #NOTE# there are negative values of this, see below. For simplicity's sake, just take anything less than 0 to be 0.
	weight=the number of times that a given decision in a given set of conditions was made
	fPerM/mPerF=the mean number of flies of the opposite sex per individual of the focal sex in a given group
	
NOTE: There are cases where there are evidently more males or females on patches than there should be in the whole experiment
	this is sometimes due to poor annotation. 
	It's sometimes due to poor assignment of flies of unknown sex. 
	This is sometimes apparently due to clickit errors (stutters etc)
	In a few cases, it's because there actually ARE too many flies in the experiment, despite my best attempts to count correctly. 
	We'll have to deal with this (though I THINK that it's not too big of a deal when we're focused on within-patch contributors to leaving.

#######################################################################
#######################################################################
#######################################################################
requires: lme4, MCMCpack

you can jump right to the final-formatted data and play along with "playingWithData.r"

######################################
Analysis of leaving data for sex ratio expt using linear, generalised linear, mixed model and bayesian regresion.

The data is processed in the folder ./clickitOut (see the relevant readme for details)

The input comprises clickitID output, plus older hand annotated data

We currently discard the 2D coordinates from ClickitID

The input file is called <provisionalCompiled.csv>
The R code is in <analyseLeaving.r>

#################################
provisionalCompiled.csv
	index=unique row id
	rep=unique id for a given hour within experiment
	month
	day
	station=recording station 1-3
	frame=variously 0:108000 or 1:107999; recorded every 1000 frames (should prob fix that)
	period=1-4: am1, am2, pm1, pm2
	genf=female genotype
	colorf=female colour
	genm=male genotype
	colorm=male colour
	nf=number of females 5,10,15 (totflies=20)
	p[%N]f=number of females on patch %N
	p[%N]m=number of males on patch %N
	sump=sum of all flies across all patches
	sumf=sum of females across all patches
	summ=sum of males across all patches 	
	[m/f]per[m/f]=the average number of males/females each male/female experiences in their group
	d[%N][m/f]=the change in the number of males/females on patch %N between the current frame and the next
##################################

We then restructure the data <"analyseLeaving.r"> to:
	examine each patch independently
		we assume flies are ignorant of the state on other patches
For each timepoint we count the number of [fe]males that stay, leave, and join a patch
	and enter each of these values in separate columns [m/f]Stay, [m/f]Leave, [m/f]Join
	df: <myByPatch>
Then we create: toAggregate with with new rows for the variables sex and leave
	with 	sex=0,1
		leave=-1, 0, 1 (bear with me, joining = -1)
		weight=[m/f]Stay, [m/f]Leave, [m/f]Join are now labelled "weight"

We aggregate (sum) $weight by $period, $genf, $genm, $nf, $pf, $pm, $sex, $leave, $offPfemales, $offPmales 
	***that is, we're counting the *individual* male and female decisions for all the states we've measured***

We duplicate the dataset---from here female and male datasets are diffferent---and:  
	discard all entries where d[%N][m/f] > 0 
		we are only going to model "leaving a patch", we don't know whether the patch-choice mechanisms have anything to do with patch leaving mechanisms
		we're missing a lot of fast-paced joining and leaving dynamics

	we now have df: maleDat and fDat

*clearly the above simplifications are not okay. but let's go with it for now.*

In aggregating this way, we lose some info re. date, rep (we might choose not to do this, eventually). The data frame is a little more tractable, sizewise, though.

maleDat:
	period=1-4: am1, am2, pm1, pm2
	gen[m/f]=[fe]male genotype
	nf=number of females 5,10,15 (totflies=20)
	p[m/f]= number of males or females on the patch (note min(pm)==1)
	sex=1
	leave=0,1
	off[F/M]=number of off-patch males or females (a very few of these are negative, either error or maybe an extra fly in the ointment: need to fix)
	weight=count of the total number of fly-frames that this decision was made
	mPerF= number of males per female for this state (simply nf/pm): note mPerM=nm-1

##################################
From here is where we do regression.  
Let's start with glm(family=binomial)

create two new columns, with pf and pm as ordinal factors: pfOrd, pmOrd

##############################
###### plot 1     ############
##############################
	fLeaveIt <- glm(leave ~ pfOrd + pmOrd + mPerF + nf + genm + period + offF, weights = weight, data = fDat, family = binomial)
	Call:
	glm(formula = leave ~ pfOrd + pmOrd + mPerF + nf + genm + period + 
	    offF, family = binomial, data = fDat, weights = weight)

		Deviance Residuals: 
		   Min      1Q  Median      3Q     Max  
		-6.675  -0.686   0.000   0.000  10.406  

		Coefficients:
			     Estimate Std. Error z value Pr(>|z|)    
		(Intercept) -3.488553   7.606780  -0.459 0.646514    
		pfOrd.L     -2.393586   0.268061  -8.929  < 2e-16 ***
		pfOrd.Q     -0.272710   0.167771  -1.625 0.104059    
		pfOrd.C      0.404478   0.254195   1.591 0.111562    
		pfOrd^4      1.084744   0.269132   4.031 5.57e-05 ***
		pfOrd^5      0.821312   0.220763   3.720 0.000199 ***
		pfOrd^6      0.415830   0.244692   1.699 0.089244 .  
		pfOrd^7      0.265529   0.272978   0.973 0.330697    
		pfOrd^8     -0.027211   0.294189  -0.092 0.926304    
		pfOrd^9     -0.357176   0.320972  -1.113 0.265797    
		pfOrd^10    -0.335263   0.293089  -1.144 0.252667    
		pfOrd^11    -0.085748   0.212505  -0.404 0.686573    
		pfOrd^12    -0.037491   0.134217  -0.279 0.779994    
		pmOrd.L     -3.484642  31.907861  -0.109 0.913036    
		pmOrd.Q     -1.158129  17.145088  -0.068 0.946145    
		pmOrd.C      2.479519   7.686384   0.323 0.747009    
		pmOrd^4      4.457882  29.679459   0.150 0.880606    
		pmOrd^5      5.626987  40.182032   0.140 0.888630    
		pmOrd^6      4.852942  37.903951   0.128 0.898123    
		pmOrd^7      3.534761  27.591526   0.128 0.898062    
		pmOrd^8      1.905887  15.733523   0.121 0.903584    
		pmOrd^9      0.774500   6.790168   0.114 0.909189    
		pmOrd^10     0.418347   1.953353   0.214 0.830416    
		mPerF        0.017510   0.047603   0.368 0.713001    
		nf          -0.069159   0.007068  -9.785  < 2e-16 ***
		genmF        0.290094   0.039862   7.278 3.40e-13 ***
		genmG       -0.198085   0.049457  -4.005 6.20e-05 ***
		period      -0.111635   0.015711  -7.105 1.20e-12 ***
		offF         0.070287   0.007542   9.320  < 2e-16 ***
		---
		Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

		(Dispersion parameter for binomial family taken to be 1)

		    Null deviance: 28111  on 12549  degrees of freedom
		Residual deviance: 26894  on 12521  degrees of freedom
		AIC: 26952

		Number of Fisher Scoring iterations: 11
		

		>anova(fLeaveIt, test="Chisq")
		Analysis of Deviance Table

		Model: binomial, link: logit

		Response: leave

		Terms added sequentially (first to last)


		       Df Deviance Resid. Df Resid. Dev  Pr(>Chi)    
		NULL                   12549      28111              
		pfOrd  12   817.16     12537      27294 < 2.2e-16 ***
		pmOrd  10   116.43     12527      27177 < 2.2e-16 ***
		mPerF   1     0.03     12526      27177    0.8564    
		nf      1    25.02     12525      27152 5.682e-07 ***
		genm    2    99.35     12523      27053 < 2.2e-16 ***
		period  1    71.92     12522      26981 < 2.2e-16 ***
		offF    1    86.89     12521      26894 < 2.2e-16 ***
		---
		Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
#######################################################################################################################################
##############################
###### plot 2     ############
##############################
	mLeaveIt <- glm(leave ~ pmOrd + pfOrd + fPerM + nf + genm + period + offM, weights = weight, data = maleDat, family = binomial)
	Call:
	glm(formula = leave ~ pmOrd + pfOrd + fPerM + nf + genm + period + 
	    offM, family = binomial, data = maleDat, weights = weight)

		Deviance Residuals: 
		    Min       1Q   Median       3Q      Max  
		-7.5954 -0.8773  0.0000  0.0000  7.5246 

		Coefficients:
			     Estimate Std. Error z value Pr(>|z|)    
		(Intercept) -3.147956   5.517496  -0.571 0.568311    
		pmOrd.L      0.638723   0.306920   2.081 0.037427 *  
		pmOrd.Q      0.031731   0.283271   0.112 0.910811    
		pmOrd.C      1.957900   0.263060   7.443 9.86e-14 ***
		pmOrd^4      0.396168   0.240417   1.648 0.099386 .  
		pmOrd^5      0.770584   0.212436   3.627 0.000286 ***
		pmOrd^6      0.241508   0.191643   1.260 0.207599    
		pmOrd^7      0.115173   0.167295   0.688 0.491176    
		pmOrd^8     -0.132970   0.129683  -1.025 0.305199    
		pmOrd^9      0.140805   0.090604   1.554 0.120167    
		pfOrd.L     -5.874878  17.920749  -0.328 0.743044    
		pfOrd.Q      0.812316   5.729834   0.142 0.887262    
		pfOrd.C      3.290668  24.265883   0.136 0.892131    
		pfOrd^4      3.211229  19.253901   0.167 0.867541    
		pfOrd^5     -0.534639   4.466114  -0.120 0.904713    
		pfOrd^6     -3.525819  24.850808  -0.142 0.887175    
		pfOrd^7     -3.185397  23.966741  -0.133 0.894265    
		pfOrd^8     -0.124379   2.920643  -0.043 0.966031    
		pfOrd^9      2.980496  21.899961   0.136 0.891745    
		pfOrd^10     4.748635  34.433105   0.138 0.890312    
		pfOrd^11     4.177338  30.825341   0.136 0.892204    
		pfOrd^12     2.707976  18.434794   0.147 0.883215    
		pfOrd^13     0.862086   6.848096   0.126 0.899821    
		fPerM        0.058641   0.029515   1.987 0.046938 *  
		nf           0.065870   0.007685   8.572  < 2e-16 ***
		genmF        0.539079   0.031403  17.166  < 2e-16 ***
		genmG       -0.629502   0.034212 -18.400  < 2e-16 ***
		period      -0.259049   0.011904 -21.761  < 2e-16 ***
		offM         0.004803   0.008333   0.576 0.564351    
		---
		Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

		(Dispersion parameter for binomial family taken to be 1)

		    Null deviance: 45098  on 14746  degrees of freedom
		Residual deviance: 40667  on 14718  degrees of freedom
		AIC: 40725

		> anova(mLeaveIt, test="Chisq")
		Analysis of Deviance Table

		Model: binomial, link: logit

		Response: leave

		Terms added sequentially (first to last)


		       Df Deviance Resid. Df Resid. Dev Pr(>Chi)    
		NULL                   14746      45098             
		pmOrd   9  1948.97     14737      43149   <2e-16 ***
		pfOrd  13   564.54     14724      42585   <2e-16 ***
		fPerM   1     2.40     14723      42582   0.1210    
		nf      1   208.15     14722      42374   <2e-16 ***
		genm    2  1207.67     14720      41166   <2e-16 ***
		period  1   498.60     14719      40668   <2e-16 ***
		offM    1     0.33     14718      40667   0.5642    
		---
		Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

################################################################################
The next couple analyses, Bayesian regression and mixed-effect models with model selection
happen in the files <mcmcpackRegression[M/F].r>

*because the Bayesian (mcmc analysis) doesn't support the "weights" argument, we first need to 
"explode" the data sets using the custom function "explodeDat()".

logmcmcM=MCMClogit(leave~pm + fPerM + genm + nf + period, burnin=1000, mcmc=50000, b0=0, B0=0.4, data=mDatExp, thin=80, seed=1)

	Iterations = 1001:50921
	Thinning interval = 80 
	Number of chains = 1 
	Sample size per chain = 625 

	1. Empirical mean and standard deviation for each variable,
	   plus standard error of the mean:

		        Mean       SD  Naive SE Time-series SE
	(Intercept) -1.29108 0.048202 0.0019281      0.0019281
	pm           0.13414 0.008361 0.0003345      0.0003345
	fPerM       -0.31469 0.014751 0.0005901      0.0005901
	genmF        0.55426 0.028344 0.0011338      0.0011338
	genmG       -0.71907 0.032751 0.0013101      0.0013101
	nf           0.04696 0.003800 0.0001520      0.0001762
	period      -0.27370 0.011509 0.0004604      0.0004604

	2. Quantiles for each variable:

		        2.5%      25%      50%      75%    97.5%
	(Intercept) -1.39263 -1.32174 -1.28845 -1.25778 -1.19882
	pm           0.11850  0.12869  0.13347  0.13939  0.15164
	fPerM       -0.34410 -0.32421 -0.31498 -0.30464 -0.28624
	genmF        0.50066  0.53464  0.55517  0.57371  0.61006
	genmG       -0.77754 -0.74210 -0.71909 -0.69699 -0.65433
	nf           0.03912  0.04452  0.04709  0.04974  0.05404
	period      -0.29636 -0.28044 -0.27361 -0.26633 -0.25046

logmcmF=MCMClogit(leave~pf + mPerF + genm + nf + period, burnin=1000, mcmc=50000, b0=0, B0=0.4, data=fDatExp, thin=80, seed=1)

	Iterations = 1001:50921
	Thinning interval = 80 
	Number of chains = 1 
	Sample size per chain = 625 

	1. Empirical mean and standard deviation for each variable,
	   plus standard error of the mean:

		        Mean       SD  Naive SE Time-series SE
	(Intercept) -1.42881 0.081077 0.0032431      0.0032431
	pf          -0.16245 0.008920 0.0003568      0.0003568
	mPerF        0.03663 0.022339 0.0008936      0.0008936
	genmF        0.33940 0.040126 0.0016050      0.0016050
	genmG       -0.12214 0.047020 0.0018808      0.0018808
	nf          -0.03957 0.005637 0.0002255      0.0002255
	period      -0.12601 0.015598 0.0006239      0.0005665

	2. Quantiles for each variable:

		        2.5%      25%      50%      75%    97.5%
	(Intercept) -1.59192 -1.47996 -1.43040 -1.37368 -1.28484
	pf          -0.17905 -0.16877 -0.16260 -0.15628 -0.14463
	mPerF       -0.01141  0.02259  0.03748  0.05202  0.07888
	genmF        0.26431  0.31321  0.33765  0.36603  0.41994
	genmG       -0.21117 -0.15619 -0.12008 -0.09102 -0.02623
	nf          -0.05004 -0.04314 -0.03979 -0.03586 -0.02867
	period      -0.15689 -0.13598 -0.12615 -0.11539 -0.09593


################################################################################
after this we do model selection in glmer

using the variable vectors:
	mVarVec=c("nf", "pf", "pm", "pf:pm", "fPerM", "offM", "(1 | genf)", "(pf | genf)", "(1 | genm)", "(pm | genm)", "period")

and 
	fVarVec=c("nf", "pf", "pm", "pf:pm", "mPerF", "offF", "(1 | genf)", "(pf | genf)", "(1 | genm)", "(pm | genm)", "period")

We always include the fixed effects "period" and "nf" (since they always come out super-significant in all other models)
	We then try all non-redundant and legal combinations of the other variables (no pf:pm without both pf and pm as main effects, e.g.)

see <playingWithData.r> to actually play with the models and data

	bestM=glmer(leave~nf + fPerM + (1 | genf) + (pm | genm) + period, data=mDat, weights=weight, family="binomial")
		Generalized linear mixed model fit by maximum likelihood (Laplace Approximation) ['glmerMod']
		 Family: binomial ( logit )
		Formula: leave ~ nf + fPerM + (1 | genf) + (pm | genm) + period
		   Data: mDat
		Weights: weight
		      AIC       BIC    logLik  deviance  df.resid 
		 42020.84  42084.53 -21002.42  42004.84     21182 
		Random effects:
		 Groups Name        Std.Dev. Corr
		 genf   (Intercept) 0.1361       
		 genm   (Intercept) 0.2556       
			pm          0.2643   0.33
		Number of obs: 21190, groups: genf, 3; genm, 3
		Fixed Effects:
		(Intercept)           nf        fPerM       period  
		   -1.58586      0.04768     -0.30273     -0.24258  
		> coef(bestM)
		$genf
		  pm (Intercept)         nf      fPerM     period
		A  0   -1.507656 0.04768039 -0.3027299 -0.2425754
		B  0   -1.500205 0.04768039 -0.3027299 -0.2425754
		C  0   -1.749461 0.04768039 -0.3027299 -0.2425754

		$genm
			   pm (Intercept)         nf      fPerM     period
		E  0.31206737   -1.770438 0.04768039 -0.3027299 -0.2425754
		F  0.33192667   -1.206937 0.04768039 -0.3027299 -0.2425754
		G -0.03308486   -1.585496 0.04768039 -0.3027299 -0.2425754

		attr(,"class")
		[1] "coef.mer"
		> ranef(bestM)
		$genf
		  (Intercept)
		A  0.07820097
		B  0.08565233
		C -0.16360399

		$genm
		    (Intercept)          pm
		E -0.1845808588  0.31206737
		F  0.3789201997  0.33192667
		G  0.0003607182 -0.03308486


	bestF=glmer(leave~nf + pf + offF + (1 | genf) + (pm | genm) + period, data=fDatExp, weights=weight, family="binomial")
		> bestF
		Generalized linear mixed model fit by maximum likelihood (Laplace Approximation) ['glmerMod']
		 Family: binomial ( logit )
		Formula: leave ~ nf + pf + offF + (1 | genf) + (pm | genm) + period
		   Data: fDatExp
		Weights: weight
		      AIC       BIC    logLik  deviance  df.resid 
		 75210.32  75290.84 -37596.16  75192.32     56760 
		Random effects:
		 Groups Name        Std.Dev. Corr
		 genf   (Intercept) 0.2954       
		 genm   (Intercept) 0.2650       
			pm          0.2322   0.61
		Number of obs: 56769, groups: genf, 3; genm, 3
		Fixed Effects:
		(Intercept)           nf           pf         offF       period  
		   -0.96759     -0.15378     -0.32113      0.05032     -0.30852  
		> coef(bestF)
		$genf
		  pm (Intercept)         nf        pf       offF     period
		A  0  -0.6692502 -0.1537841 -0.321135 0.05032432 -0.3085172
		B  0  -0.8968661 -0.1537841 -0.321135 0.05032432 -0.3085172
		C  0  -1.3365962 -0.1537841 -0.321135 0.05032432 -0.3085172

		$genm
			  pm (Intercept)         nf        pf       offF     period
		E -0.1297819  -1.3063408 -0.1537841 -0.321135 0.05032432 -0.3085172
		F  0.2636644  -0.7152512 -0.1537841 -0.321135 0.05032432 -0.3085172
		G -0.2719318  -0.9765366 -0.1537841 -0.321135 0.05032432 -0.3085172

		attr(,"class")
		[1] "coef.mer"
		> ranef(bestF)
		$genf
		  (Intercept)
		A  0.29833509
		B  0.07071925
		C -0.36901086

		$genm
		   (Intercept)         pm
		E -0.338755490 -0.1297819
		F  0.252334158  0.2636644
		G -0.008951298 -0.2719318


######################################################
And now let's play with MATING DATA!



