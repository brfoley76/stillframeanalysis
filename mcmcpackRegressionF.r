setwd("/media/brad/Data/Documents/VideoAnalysis/stillFrames/stillframeanalysis")
fDat=read.table("fDat.csv", sep=",", header=T, as.is=T)
library(lme4)
library(MCMCpack)


explodeDat=function(myDat){
  expDat=myDat[rep(1,myDat[,"weight"]),]
  return(expDat)  
}

fDatExp=do.call(rbind, lapply(1:nrow(fDat), function(k) {explodeDat(fDat[k,])}))

logmcmF=MCMClogit(leave~pf + mPerF + genm + nf + period, burnin=1000, mcmc=10000, b0=0, B0=0.4, data=fDatExp, thin=10, seed=1)
logmcmcF2=MCMClogit(leave~pf + pm + period, burnin=2500, mcmc=10000, b0=0, B0=0, data=fDatExp, thin=100, seed=2)


plot(as.numeric(logmcmcF[,"pf"])~c(1:length(as.numeric(logmcmcF[,"pf"]))))
trend=lm(as.numeric(logmcmcF[,"pf"])~c(1:length(as.numeric(logmcmcF[,"pf"]))))
summary(trend)
abline(trend, col="red")



plot(as.numeric(logmcmcF[,"pf"])~c(1:length(as.numeric(logmcmcF[,"pm"]))))

bbb=cor(mDatExp[,1:8])




makeColumn=function(k, numRows){
  repLength=numRows/(2^(k))  
  oneVec=rep(1, repLength)
  zeroVec=rep(0, repLength)
  repVec=c(zeroVec, oneVec)
  colVec=rep(repVec, numRows/(repLength*2))
  return(colVec)
}

makeFactorialMatrix=function(lengthVec){
  numRows=2^lengthVec
  factMat=do.call(cbind, lapply(1:lengthVec, function(k) {makeColumn(k, numRows)}))
}

#####Looks non-terrible, though the data is fucked six ways from Sunday
####Next, model selection and plots.

fVarVec=c("nf", "pf", "pm", "pf:pm", "mPerF", "offF", "(1 | genf)", "(pf | genf)", "(1 | genm)", "(pm | genm)", "period")
allCombosF=makeFactorialMatrix(length(fVarVec))
colnames(allCombosF)=fVarVec

allCombosF=allCombosF[c(allCombosF[,"period"]==1),]
allCombosF=allCombosF[c(allCombosF[,"nf"]==1),]

zzz=rowSums(allCombosF[,c(4, 5)]) ##no both pf:pm fperm 
allCombosF=allCombosF[zzz<2,]

zzz=c(c(allCombosF[,"pf"]*allCombosF[,"pm"] == 0) & allCombosF[,"pf:pm"]==1)
zzz=!(zzz)
allCombosF=allCombosF[zzz,]

zzz=rowSums(allCombosF[,c(7:10)]) ###at least one random effect
NoRandom=allCombosF[c(zzz==0),]
zzz=zzz>0
allCombosF=allCombosF[zzz,]

zzz=c(c(allCombosF[,"(1 | genf)"]+allCombosF[,"(pf | genf)"])<2 & c(allCombosF[,"(1 | genf)"]+allCombosF[,"(pf | genf)"])<2)
allCombosF=allCombosF[zzz,]



modelInfo=c()
#FMOD "fmodx=glmer(leave~nf + pf + pm + pf:pm +(pf | genf) + (1+ pm | genm) + period, data=mDatExp, family=\"binomial")"
fFormVec=c("nf + ", "pf + ", "pm + ", "pf:pm + ", "mPerF + ", "offF + ", "(1 | genf) + ", "(pf | genf) + ", "(1 | genm) + ", "(pm | genm) + ", "period")

for(i in 1:nrow(allCombosF)){
  modName=paste("fmod", i, sep="")
  modTextVec=fFormVec[allCombosF[i,]==1]
  cat(modName, "=glmer(leave~", file="modEncodeF.r", sep="")
  cat(modTextVec, file="modEncodeF.r", sep="", append=T)
  cat(", data=fDat, weight=weight, family=\"binomial\")\nmodFitLine=c(modName, summary(", modName,")$AICtab)", file="modEncodeF.r", sep="", append=T)
  source("modEncodeF.r")
  cat(modName, modTextVec, modFitLine, "\n", file="resultsOfModelSelF.txt", append=T, sep=",")
}
####no random effects:
for(i in 1:nrow(NoRandom)){
  modName=paste("fmod", i, sep="")
  modTextVec=fFormVec[NoRandom[i,]==1]
  cat(modName, "=glm(leave~", file="modEncodeF.r", sep="")
  cat(modTextVec, file="modEncodeF.r", sep="", append=T)
  cat(", data=fDat, weights=weight, family=\"binomial\")", file="modEncodeF.r", sep="", append=T)
  cat("\nmodFitLine=c(modName, AIC=AIC(", modName,"), BIC=BIC(", modName,"), logLik=as.numeric(logLik(",modName,")),", file="modEncodeF.r", sep="", append=T)
  cat("deviance=summary(",modName,")$deviance, df.residual=summary(",modName,")$df.residual)",file="modEncodeF.r", sep="", append=T) 
  source("modEncodeF.r")
  cat(modName, modTextVec, modFitLine, "\n", file="resultsOfModelSelF.txt", append=T, sep=",")
}


myFModResults=read.csv("resultsOfModelSelF.txt", header=F)
myFModResults=myFModResults[,c(1, 2, 4:8)]
colnames(myFModResults)=c("modName", "model", "AIC", "BIC", "logLik", "deviance", "dfResid")

myFModsRes=myFModResults[order(myFModResults[,"BIC"]),]
head(myFModsRes, 10)

fLeaveRes=glmer(leave~ nf+pf+mPerF+offF+(1|genf)+(pm|genm)+period, data=fDat, weights=weight, family="binomial")



display(fmod79)
coef(fmod79)
ranef(fmod79)

display(fmod28)
coef(fmod28)
ranef(fmod28)

display(fmod111)
coef(fmod111)
ranef(fmod111)
