setwd("/media/brad/Data/Documents/VideoAnalysis/stillFrames/stillframeanalysis")
mDat=read.table("mDat.csv", sep=",", header=T, as.is=T)
library(MCMCpack)
library(lme4)

explodeDat=function(myDat){
  expDat=myDat[rep(1,myDat[,"weight"]),]
  return(expDat)  
}

mDatExp=do.call(rbind, lapply(1:nrow(mDat), function(k) {explodeDat(mDat[k,])}))

logmcmcM=MCMClogit(leave~pm + fPerM + genm + nf + period, burnin=1000, mcmc=50000, b0=0, B0=0.4, data=mDatExp[c(mDatExp[, "pm"] > 0 ), ], thin=80, seed=1)
#logmcmcM2=MCMClogit(leave~pf + pm, burnin=2500, mcmc=50000, b0=0, B0=0, data=mDatExp, thin=80, seed=2)
logmcmF=MCMClogit(leave~pf + mPerF + genm + nf + period, burnin=1000, mcmc=50000, b0=0, B0=0.4, data=fDatExp, thin=80, seed=1)
#logmcmcF2=MCMClogit(leave~pf + pm + period, burnin=2500, mcmc=50000, b0=0, B0=0, data=fDatExp, thin=80, seed=2)

par(mfrow=c(2,1))

plot(as.numeric(logmcmcM[,"pm"])~c(1:length(as.numeric(logmcmcM[,"pm"]))))
trend=lm(as.numeric(logmcmcM[,"pm"])~c(1:length(as.numeric(logmcmcM[,"pm"]))))
summary(trend)
abline(trend, col="red")

plot(as.numeric(logmcmcM[,"pm"])~c(1:length(as.numeric(logmcmcF[,"pf"]))))

bbb=cor(mDatExp[,1:8])


aaa=glmer(leave~nf + pf + pm + pf:pm +(pf | genf) + (1+ pm | genm) + period, data=mDatExp, family="binomial")

display(aaa)
coef(aaa)
ranef(aaa)

plot(aaa)

makeColumn=function(k, numRows){
  repLength=numRows/(2^(k))  
  oneVec=rep(1, repLength)
  zeroVec=rep(0, repLength)
  repVec=c(zeroVec, oneVec)
  colVec=rep(repVec, numRows/(repLength*2))
  return(colVec)
}

makeFactorialMatrix=function(lengthVec){
  numRows=2^lengthVec
  factMat=do.call(cbind, lapply(1:lengthVec, function(k) {makeColumn(k, numRows)}))
}

#####Looks non-terrible, though the data is fucked six ways from Sunday
####Next, model selection and plots.

mVarVec=c("nf", "pf", "pm", "pf:pm", "fPerM", "offM", "(1 | genf)", "(pf | genf)", "(1 | genm)", "(pm | genm)", "period")
allCombos=makeFactorialMatrix(length(mVarVec))
colnames(allCombos)=mVarVec

allCombos=allCombos[c(allCombos[,"period"]==1),]
allCombos=allCombos[c(allCombos[,"nf"]==1),]

zzz=rowSums(allCombos[,c(4, 5)]) ##no both pf:pm fperm DAMMIT - forgot to restirct models with "pf:pm" to those also with "pf + pm"
allCombos=allCombos[zzz<2,]

zzz=c(c(allCombos[,"pf"]*allCombos[,"pm"] == 0) & allCombos[,"pf:pm"]==1)
zzz=!(zzz)
allCombos=allCombos[zzz,]

zzz=rowSums(allCombos[,c(7:10)]) ###at least one random effect
NoRandom=allCombos[c(zzz==0),]
zzz=zzz>0
allCombos=allCombos[zzz,]

zzz=c(c(allCombos[,"(1 | genf)"]+allCombos[,"(pf | genf)"])<2 & c(allCombos[,"(1 | genf)"]+allCombos[,"(pf | genf)"])<2)
allCombos=allCombos[zzz,]

modelInfo=c()
#FMOD "fmodx=glmer(leave~nf + pf + pm + pf:pm +(pf | genf) + (1+ pm | genm) + period, data=mDatExp, family=\"binomial")"
mFormVec=c("nf + ", "pf + ", "pm + ", "pf:pm + ", "fPerM + ", "offM + ", "(1 | genf) + ", "(pf | genf) + ", "(1 | genm) + ", "(pm | genm) + ", "period")

for(i in 1:nrow(allCombos)){
  modName=paste("mmod", i, sep="")
  modTextVec=mFormVec[allCombos[i,]==1]
  cat(modName, "=glmer(leave~", file="modEncodeM.r", sep="")
  cat(modTextVec, file="modEncodeM.r", sep="", append=T)
  cat(", data=mDat, weights=weight, family=\"binomial\")\nmodFitLine=c(modName, summary(", modName,")$AICtab)", file="modEncodeM.r", sep="", append=T)
  source("modEncodeM.r")
  cat(modName, modTextVec, modFitLine, "\n", file="resultsOfModelSelM.txt", append=T, sep=",")
}

for(i in 1:nrow(NoRandom)){
  modName=paste("mmod", i, sep="")
  modTextVec=mFormVec[NoRandom[i,]==1]
  cat(modName, "=glm(leave~", file="modEncodeM.r", sep="")
  cat(modTextVec, file="modEncodeM.r", sep="", append=T)
  cat(", data=mDat, weights=weight, family=\"binomial\")", file="modEncodeM.r", sep="", append=T)
  cat("\nmodFitLine=c(modName, AIC=AIC(", modName,"), BIC=BIC(", modName,"), logLik=as.numeric(logLik(",modName,")),", file="modEncodeM.r", sep="", append=T)
  cat("deviance=summary(",modName,")$deviance, df.residual=summary(",modName,")$df.residual)",file="modEncodeM.r", sep="", append=T) 
  source("modEncodeM.r")
  cat(modName, modTextVec, modFitLine, "\n", file="resultsOfModelSelM.txt", append=T, sep=",")
}

myMModResults=read.csv("resultsOfModelSelM.txt", header=F)
colnames(myMModResults)=c("modName", "model", "dupName", "AIC", "BIC", "logLik", "deviance", "dfResid")
myMModResults=myMModResults[,c(1, 2, 4:8)]

myMModResults=myMModResults[order(myMModResults[,"BIC"]),]
head(myMModResults, 20)

maleLeaveRes=glmer(leave~nf+pf+fPerM+offM+(1|genf)+(pm|genm)+period , data=mDat, weights=weight, family="binomial")
maleLeaveRes2=glmer(leave~nf+fPerM+offM+(1|genf)+(pm|genm)+period , data=mDat, weights=weight, family="binomial")
maleLeaveRes3=glmer(leave~nf+fPerM+(1|genf)+(pm|genm)+period, data=mDat, weights=weight, family="binomial")

mDatAgg=aggregate(mDat[,c("weight")]*mDat[,"leave"], by=list(mDat[,"genm"], mDat[,"genf"], mDat[,"nf"]), FUN="mean")
boxplot(mDatAgg[,"x"]~mDatAgg[,"Group.1"]+mDatAgg[,"Group.3"])

fDatAgg=aggregate(fDat[,c("weight")]*fDat[,"leave"], by=list(fDat[,"genm"], fDat[,"genf"], fDat[,"nf"]), FUN="mean")
boxplot(fDatAgg[,"x"]~fDatAgg[,"Group.2"]+fDatAgg[,"Group.3"])


