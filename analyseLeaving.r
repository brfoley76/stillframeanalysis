###############################################################
### From here, we aggregate our data files into mDat and fDat
### So that a) they're suitable for logistic regression 
###         b) they're not ridiculously large
############################################################
###########################################################
setwd("/media/brad/Data/Documents/VideoAnalysis/stillFrames/stillframeanalysis")
myData=read.table("./clickitOut/finalByFrame.csv", sep=",", as.is=T, header=T)
#### I'm going to "NA" out all the joining/leaving data from the last frame in each hour.
#### This is a bit of a hack, because for some of these, we have information from the next consecutive hour (between periods 1 and 2; and 3 and 4)
#### But to specify all the exceptions will be more work than I want to bother with right now.

myMetaData=myData[,1:12]
myByPatch=c()

lengthData=nrow(myData)
d0f=myData[2:lengthData,"p0f"]-myData[1:(lengthData-1),"p0f"]
d0f=c(d0f, NA)
d0m=myData[2:lengthData,"p0m"]-myData[1:(lengthData-1),"p0m"]
d0m=c(d0m, NA)
d1f=myData[2:lengthData,"p1f"]-myData[1:(lengthData-1),"p1f"]
d1f=c(d1f, NA)
d1m=myData[2:lengthData,"p1m"]-myData[1:(lengthData-1),"p1m"]
d1m=c(d1m, NA)
d2f=myData[2:lengthData,"p2f"]-myData[1:(lengthData-1),"p2f"]
d2f=c(d2f, NA)
d2m=myData[2:lengthData,"p2m"]-myData[1:(lengthData-1),"p2m"]
d2m=c(d2m, NA)
d3f=myData[2:lengthData,"p3f"]-myData[1:(lengthData-1),"p3f"]
d3f=c(d3f, NA)
d3m=myData[2:lengthData,"p3m"]-myData[1:(lengthData-1),"p3m"]
d3m=c(d3m, NA)

naVec=myData[,"frame"]>107000
d0f[naVec]=NA
d0m[naVec]=NA
d1f[naVec]=NA
d1m[naVec]=NA
d2f[naVec]=NA
d2m[naVec]=NA
d3f[naVec]=NA
d3m[naVec]=NA
myData[,"d0f"]=d0f
myData[,"d0m"]=d0m
myData[,"d1f"]=d1f
myData[,"d1m"]=d1m
myData[,"d2f"]=d2f
myData[,"d2m"]=d2m
myData[,"d3f"]=d3f
myData[,"d3m"]=d3m
myData[,"nf"]=as.numeric(myData[,"nf"])
offPmales=20-myData[,"nf"]-myData[,"summ"]
offPfemales=myData[,"nf"]-myData[,"sumf"]
myData=data.frame(myData, offPfemales, offPmales)

#myData[which(myData[,"offPfemales"]<0),]
#myData[which(myData[,"offPmales"]<0),]

myByPatch=c()
for(i in 0:3){
  myRestruct=data.frame(myMetaData, offPfemales, offPmales, pf=myData[,i*2+13], pm=myData[,i*2+14], df=myData[,i*2+28], dm=myData[,i*2+29])
  myByPatch=rbind(myByPatch, myRestruct)
}

myByPatch=data.frame(myByPatch, count=1, fStay=0, mStay=0, fLeave=0,mLeave=0, fJoin=0, mJoin=0)
myByPatch[which(myByPatch[,"df"]>=0),"fStay"]=myByPatch[which(myByPatch[,"df"]>=0),"pf"]
myByPatch[which(myByPatch[,"df"]<0),"fStay"]=myByPatch[which(myByPatch[,"df"]<0),"pf"]+myByPatch[which(myByPatch[,"df"]<0),"df"]
myByPatch[which(myByPatch[,"dm"]>=0),"mStay"]=myByPatch[which(myByPatch[,"dm"]>=0),"pm"]
myByPatch[which(myByPatch[,"dm"]<0),"mStay"]=myByPatch[which(myByPatch[,"dm"]<0),"pm"]+myByPatch[which(myByPatch[,"dm"]<0),"dm"]
myByPatch[which(myByPatch[,"df"]<0),"fLeave"]=myByPatch[which(myByPatch[,"df"]<0),"df"]*(-1)
myByPatch[which(myByPatch[,"dm"]<0),"mLeave"]=myByPatch[which(myByPatch[,"dm"]<0),"dm"]*(-1)
myByPatch[which(myByPatch[,"df"]>=0),"fJoin"]=myByPatch[which(myByPatch[,"df"]>=0),"df"]
myByPatch[which(myByPatch[,"dm"]>=0),"mJoin"]=myByPatch[which(myByPatch[,"dm"]>=0),"dm"]

aaa=myByPatch[which(myByPatch[,"dm"]<0),"pm"]+myByPatch[which(myByPatch[,"dm"]<0),"dm"]
bbb=aaa<0
ccc=myByPatch[which(myByPatch[,"dm"]<0),]

toAggregate=c()
toAggregateMeta=myByPatch[1:19]
toAggregate=data.frame(toAggregateMeta, sex=0, leave=0, weight=myByPatch[,"fStay"])
toAggregateChunk=data.frame(toAggregateMeta, sex=0, leave=1, weight=myByPatch[,"fLeave"])
toAggregate=rbind(toAggregate, toAggregateChunk)
toAggregateChunk=data.frame(toAggregateMeta, sex=1, leave=0, weight=myByPatch[,"mStay"])
toAggregate=rbind(toAggregate, toAggregateChunk)
toAggregateChunk=data.frame(toAggregateMeta, sex=1, leave=1, weight=myByPatch[,"mLeave"])
toAggregate=rbind(toAggregate, toAggregateChunk)
toAggregateChunk=data.frame(toAggregateMeta, sex=0, leave=-1, weight=myByPatch[,"fJoin"])
toAggregate=rbind(toAggregate, toAggregateChunk)
toAggregateChunk=data.frame(toAggregateMeta, sex=1, leave=-1, weight=myByPatch[,"mJoin"])
toAggregate=rbind(toAggregate, toAggregateChunk)

toAggSub=data.frame(toAggregate, samp=c(1:8))
toAggSubVec=toAggSub[,"samp"] %/% 8
toAggSub=toAggSub[c(toAggSubVec == 1),]

myAgg=aggregate(toAggregate[,"weight"], by=list(toAggregate[,"period"], toAggregate[,"genf"], toAggregate[,"genm"], toAggregate[,"nf"], toAggregate[,"pf"], toAggregate[,"pm"], toAggregate[,"sex"], toAggregate[,"leave"], toAggregate[,"offPfemales"], toAggregate[,"offPmales"]), FUN="sum")
names(myAgg)=c("period", "genf", "genm", "nf", "pf", "pm", "sex", "leave", "offF", "offM", "weight")
myAgg[,"nf"]=as.numeric(myAgg[,"nf"])
head(myAgg)

myAggSub=aggregate(toAggSub[,"weight"], by=list(toAggSub[,"period"], toAggSub[,"genf"], toAggSub[,"genm"], toAggSub[,"nf"], toAggSub[,"pf"], toAggSub[,"pm"], toAggSub[,"sex"], toAggSub[,"leave"], toAggSub[,"offPfemales"], toAggSub[,"offPmales"]), FUN="sum")
names(myAggSub)=c("period", "genf", "genm", "nf", "pf", "pm", "sex", "leave", "offF", "offM", "weight")
myAggSub[,"nf"]=as.numeric(myAggSub[,"nf"])
head(myAggSub)

maleDat=myAgg[which(myAgg[,"leave"]>=0 & myAgg[,"sex"]==1 ),]
maleDat=maleDat[maleDat[,"pm"]>0,]
fPerM=maleDat[,"pf"]/maleDat[,"pm"]
maleDat=data.frame(maleDat, fPerM)
fDat=myAgg[which(myAgg[,"leave"]>=0 & myAgg[,"sex"]==0 ),]
fDat=fDat[fDat[,"pf"]>0,]
mPerF=fDat[,"pm"]/fDat[,"pf"]
fDat=data.frame(fDat, mPerF)


write.table(fDat, "fDat.csv", sep=",", row.names=F)
write.table(maleDat, "mDat.csv", sep=",", row.names=F)
#fDat[fDat[,"offF"]<0,"offF"]=0
#fDat[fDat[,"offM"]<0,"offM"]=0
#maleDat[maleDat[,"offF"]<0,"offF"]=0
#maleDat[maleDat[,"offM"]<0,"offM"]=0

#create ordinal vars for pf and pm
fDat=fDat[order(fDat[,"pf"], decreasing=F),]
pfOrd=factor(fDat[,"pf"], ordered=T)
fDat=data.frame(fDat, pfOrd)
fDat=fDat[order(fDat[,"pm"], decreasing=F),]
pmOrd=factor(fDat[,"pm"], ordered=T)
fDat=data.frame(fDat, pmOrd)

fLeaveIt <- glm(leave ~ pfOrd + pmOrd + mPerF + nf + genm + period + offF, weights = weight, data = fDat, family = binomial)
par(mfrow=c(2, 2))
plot(fLeaveIt)
dev.copy(png, file="1_fOrdReg.png", width=800, height=800)
dev.off()
summary(fLeaveIt)
anova(fLeaveIt, test="Chisq")

maleDat=maleDat[order(maleDat[,"pf"], decreasing=F),]
pfOrd=factor(maleDat[,"pf"], ordered=T)
maleDat=data.frame(maleDat, pfOrd)
maleDat=maleDat[order(maleDat[,"pm"], decreasing=F),]
pmOrd=factor(maleDat[,"pm"], ordered=T)
maleDat=data.frame(maleDat, pmOrd)

mLeaveIt <- glm(leave ~ pmOrd + pfOrd + fPerM + nf + genm + period + offM, weights = weight, data = maleDat, family = binomial)
par(mfrow=c(2, 2))
plot(mLeaveIt)
dev.copy(png, file="2_mOrdReg.png", width=800, height=800)
dev.off()
summary(mLeaveIt)
anova(mLeaveIt, test="Chisq")

#fDat[fDat[,"offF"]<0,"offF"]=0
#fDat[fDat[,"offM"]<0,"offM"]=0
#maleDat[maleDat[,"offF"]<0,"offF"]=0
#maleDat[maleDat[,"offM"]<0,"offM"]=0

